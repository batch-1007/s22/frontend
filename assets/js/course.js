//window.location.search returns the query string part of the URL

//console.log(window.location.search)// pang check

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem('token')

//console.log(courseId) pang check
let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDescription")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("enrollContainer")

fetch(`http://localhost:4000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data)

	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML =

	`
		<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>
	`
})

document.querySelector("#enrollButton").addEventListener("click", (e) => {

	e.preventDefault()
	//insert the course to our course collection
	fetch('http://localhost:4000/api/users/enroll', {

		method: 'POST',
		headers:{
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},

		body: JSON.stringify({
			course: courseId
		})
	})

	.then(res =>{
		return res.json()
	})
	.then(data =>{
		//creation of new course succesful
		if(data === true){
			//redirect to coursepage
			alert('Thank you for enrolling! See you!')
			window.location.replace("./course.html")
		}else{
			//redirect in creating course
			alert("something went wrong")
		}
	})

})